package az.ingress.tdd1.Services;

import lombok.Data;

@Data
public class GameSteps {

    private Character player;
    private int x;
    private int y;
}
