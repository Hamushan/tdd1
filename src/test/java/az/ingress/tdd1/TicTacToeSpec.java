package az.ingress.tdd1;

import az.ingress.tdd1.Services.TicTacToe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

public class TicTacToeSpec {
    private TicTacToe ticTacToe;

    @BeforeEach
    void init() {
        System.out.println("Before each");
        ticTacToe = new TicTacToe();
    }

    @Test
    void whenXIsOutsideTheBoardThenException() {
        int x = -1;
        int y = 0;
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");

    }

    @Test
    void whenYIsOutsideTheBoardThenException() {
        int x = 1;
        int y = 5;
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside board");

    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        ticTacToe.play(2, 1);
        assertThatThrownBy(() -> ticTacToe.play(2, 1))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Board is occupied");
    }

    @Test
    public void whenOccupiedThenRuntimeException1() {
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
    }

    @Test
    public void whenStartThenNextPlayerX() {
        assertThat(ticTacToe.nextPlayer()).isEqualTo('x');
    }
    @Test
    void givenLastTurnWasXWhenNextPlayerThenO() {
        ticTacToe.play(1, 1);
        assertThat(ticTacToe.nextPlayer()).isEqualTo('o');
    }
    @Test
    void givenLastTurnWasOWhenNextPlayerThenX() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 3);
        assertThat(ticTacToe.nextPlayer()).isEqualTo('x');
    }

    @Test
    public void whenPlayThenNoWinner() {
//        assertThat(ticTacToe.play(1, 1)).isEqualTo("No winner");
        assertThat("no winner").isEqualTo(ticTacToe.play(1, 1));
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O
        assertThat(ticTacToe.play(3, 1)).isEqualTo("x is the winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        assertThat(ticTacToe.play(1, 3)).isEqualTo("o is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // O
        assertThat(ticTacToe.play(3, 3)).isEqualTo("x is the winner");

    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 2); // O
        assertThat(ticTacToe.play(3, 1)).isEqualTo("x is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);

        assertThat(ticTacToe.play(3, 2)).isEqualTo("The result is draw");
    }

}
